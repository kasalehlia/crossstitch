#!/usr/bin/env python2
import copy
import png
import sys
import traceback

import crossstitch as cs
from PyEmb import Embroidery, Point

def start(png_file, background):

    # READ INPUT
    reader = png.Reader(open(png_file, 'rb'))
    (width, height, pixels, metadata) = reader.read_flat()
    if 'palette' in metadata:
        print "palette found, %d colors" % len(metadata['palette'])
        #print metadata['palette']
        # FIND SECTIONS
        sections = cs.findSections(width, pixels, background)
    else:
        print "PNG without color palette are not supported"
        sys.exit(2)

    # PROCESS SECTIONS
    output = list(buildPaths(sections, width, pixels))
    print cs.used

    printAsSvg("out.svg", output, metadata['palette'])
    return

    # WRITE OUT
    paths = dict()
    # group by color
    for color,path in buildPaths(sections, width, pixels):
        if color not in paths:
            paths[color] = []
        paths[color].append(path)
    # convert to stiches
    e = Embroidery()
    for color,paths in paths.iteritems():
        #TODO (bottleneck) TSP for section order
        for path in paths:
            for stitch in path:
                p = Point(stitch[0],-stitch[1])
                p.color = color
                e.addStitch(p)
    print e.export_melco(None)

def buildPaths(sections, width, pixels):
    """generator yielding each section with its color
    and the calculated path"""
    data = list(cs.chunks(pixels, width))
    for section in sections:
        j,i = next(iter(section))
        color = data[i][j]
        yield (color, cs.sectionToPath(section))

def printAsSvg(svgFile, paths, palette):
    """exports stitching to SVG"""
    svg = open(svgFile,"w+")
    out = ""
    maxX, maxY = (0,0)
    stitches = 0
    for i in paths:
        if i[1] is None or len(i[1]) == 0:
          continue
        out += '  <polyline points="'
        for dx,dy in i[1]:
            out += str(dx*10)+","+str(dy*10)+" "
            stitches += 1
        maxX = max(maxX, max(i[1],key=lambda a: a[0])[0])
        maxY = max(maxY, max(i[1],key=lambda a: a[1])[1])
        color = "rgb"+str(palette[i[0]])
        out +='"\n  style="fill:none;stroke:'+color+'" />'
    svg.write('<svg height="'+str(maxY*10)+'" width="'+str(maxX*10)+'">\n')
    svg.write(out)
    svg.write('\n</svg>')
    svg.close()
    print "%d stitches" % stitches

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "USAGE: %s <PNG file> <background color index>" % sys.argv[0]
        sys.exit(1)
    start(sys.argv[1], int(sys.argv[2]))

