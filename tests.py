import unittest
import crossstitch as cs

class TestLineNormalization(unittest.TestCase):

  def testEmptySection(self):
    self.assertIsNone(cs.normalizeLine(set([])))

  def testSinglePixel(self):
    self.assertIsNone(cs.normalizeLine(set([(1,1)])))

  def testShortLine(self):
    sec = [(1,1),(1,2)]
    line = cs.normalizeLine(set(sec))
    self.assertTrue(line == sec or line == list(reversed(sec)))

  def testSimpleLine(self):
    sec = [(1,1),(1,2),(2,3),(2,4)]
    line = cs.normalizeLine(set(sec))
    self.assertTrue(line == sec or line == list(reversed(sec)))

  def testCorneredLine(self):
    sec = [(1,1),(1,2),(1,3),(2,3),(3,3)]
    line = cs.normalizeLine(set(sec))
    self.assertTrue(line == sec or line == list(reversed(sec)))

  def testAcuteLine(self):
    sec = [(1,1),(1,2),(1,3),(1,4),(2,3),(3,2),(4,1)]
    line = cs.normalizeLine(set(sec))
    self.assertTrue(line == sec or line == list(reversed(sec)))

  def testMultiline(self):
    sec = [(1,1),(1,2),(1,3),(2,2),(2,3)]
    line = cs.normalizeLine(set(sec))
    self.assertIsNone(line)

  def testArea(self):
    sec = [(1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3)]
    line = cs.normalizeLine(set(sec))
    self.assertIsNone(line)

class TestAdjancencyDetection(unittest.TestCase):

  def testSetAdjancency(self):
    s = set([(1,1),(1,2),(1,3)])
    self.assertItemsEqual(cs.getAdjacent(s,(1,1)),[(1,2)])
    self.assertItemsEqual(cs.getAdjacent(s,(1,2)),[(1,1),(1,3)])

  def testListAdjancency(self):
    l = range(5)
    self.assertItemsEqual(cs.getAdjacent(l,0),[1])
    self.assertItemsEqual(cs.getAdjacent(l,3),[2,4])


if __name__ == '__main__':
  unittest.main()
