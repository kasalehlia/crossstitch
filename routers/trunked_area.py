def rate(sec):
    flipped = False
    x_min, x_max, y_min, y_max = _boundaries(sec)
    height = y_max - y_min + 1
    # try to find two columns going all the way (requisite)
    for x in range(x_min, x_max):
        if len(_pixel_in_column(sec, x)) == height and len(_pixel_in_column(sec, x+1)) == height:
            break
    else:
        # flip and try again (no need to flip back)
        sec = _flip(sec)
        flipped = True
        x_min, x_max, y_min, y_max = _boundaries(sec)
        height = y_max - y_min + 1
        for x in range(x_min, x_max):
            if len(_pixel_in_column(sec, x)) == height and len(_pixel_in_column(sec, x+1)) == height:
                break
        else:
            return 0
    sec = set(sec)
    for cy in range(y_min, y_max+1):
        # walk left
        for cx in range(x, x_min-1, -1):
            if (cx, cy) in sec:
                sec.discard((cx,cy))
            else:
                break
        # walk right
        for cx in range(x+1, x_max+1):
            if (cx, cy) in sec:
                sec.discard((cx,cy))
            else:
                break
    if len(sec) == 0:
        # extra lines: 2*height
        return 100, x, flipped
    else:
        return 0

def route(sec, trunk, flipped):
    sec = set(_flip(sec)) if flipped else set(sec)
    x_min, x_max, y_min, y_max = _boundaries(sec)
    path = [(trunk+1,y_min)]
    # go down right trunk, walk to the right and back to the trunk
    for cy in range(y_min, y_max+1):
        ff = 0 # flipflop
        # walk right
        stop = x_max
        for cx in range(trunk+1, x_max+2): # deliberately walk too far
            ff = not ff
            if (cx,cy) in sec:
                path.append((cx+1,cy+ff))
            else:
                path.append((cx,cy+ff))
                stop = cx-1
                break
        # walk back
        for cx in range(stop, trunk, -1):
            ff = not ff
            path.append((cx,cy+ff))
    assert ff == 1
    for cy in range(y_max, y_min-1, -1):
        ff = 1
        # walk left
        stop = x_min
        for cx in range(trunk, x_min-2, -1):
            ff = not ff
            if (cx,cy) in sec:
                path.append((cx,cy+ff))
            else:
                path.append((cx+1,cy+ff))
                stop = cx+1
                break
        # walk back
        for cx in range(stop, trunk+1):
            ff = not ff
            path.append((cx+1,cy+ff))
    if flipped:
        path = [(t[1],t[0]) for t in path]
    return path



def _pixel_in_column(sec, x):
    """returns a set of pixels in a specific column"""
    return [p for p in sec if p[0] == x]

def _boundaries(sec):
    return  min(sec, key=lambda a: a[0])[0], max(sec, key=lambda a: a[0])[0], min(sec, key=lambda a: a[1])[1], max(sec, key=lambda a: a[1])[1]

def _flip(sec):
    return [t.swap() for t in sec]

