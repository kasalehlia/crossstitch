
def rate(sec):
    return (len(sec) in [0,1])*100

def route(sec):
    if len(sec) == 0:
        return []
    if len(sec) == 1:
        a = iter(sec).next()
        return [a,a+(1,1),a+(0,1),a+(1,0)]
