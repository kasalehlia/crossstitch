import routers as routers_module
routers = [getattr(routers_module,r) for r in routers_module.__all__]

used = dict(zip([r.__name__ for r in routers], [0]*len(routers)))

def sectionToPath(sec):
    """calculates the stitching path for this section,
    returning a list of positions to be stitched"""
    def t(val):
        return val if type(val) == tuple else (val,)
    options = sorted([(r,)+t(r.rate(sec)) for r in routers], key=lambda x: x[1], reverse=True)
    router = options[0]
    used[router[0].__name__] += 1
    return router[0].route(sec, *router[2:])

# SECTIONS

def findSections(width, pixels, background):
    """searches the image for same color sections with an 8-way floodfill"""
    data = list(chunks(pixels, width))
    sections = []
    while len([0 for line in data if line.count(background) < len(line)]) > 0:
        for i in xrange(len(data)):
            for j in xrange(width):
                if data[i][j] != background:
                    section = set()
                    _findSingleSection(data, i, j, section, background)
                    sections.append(section)
    print "found %d sections" % len(sections)
    #for s in sections: print s
    return sections

def _findSingleSection(data, i, j, section, background):
    stack = [(j,i)]
    color = data[i][j]
    while len(stack) > 0:
        j,i = stack.pop()
        section.add(T(j,i)) # row,col opposed to x,y
        data[i][j] = background # set to background color to mark as "done"
        for off_i in [-1,0,1]:
            c_i = max(0,min(i+off_i,len(data)-1))
            for off_j in [-1,0,1]:
                c_j = max(0,min(j+off_j,len(data[0])-1))
                if data[c_i][c_j] == color and (c_j,c_i) not in stack and (c_j,c_i) not in section:
                    stack.append((c_j,c_i))


# HELPERS

def chunks(l, n):
    """splits an iterable into *n*-sized chunks"""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def T(a,b):
    return Tp((a,b))

class Tp(tuple):
    def __add__(self, other):
        return Tp(x + y for x, y in zip(self, other))

    def __sub__(self, other):
        return Tp(x - y for x, y in zip(self, other))

    def abs(self):
        return Tp(abs(c) for c in self)

    def swap(self):
        return Tp(reversed(self))

